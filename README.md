# Java Wrapper for Investec Programmable Banking Open API

This project contains two examples of wrapper classes for the Investec API

## OkHttpClient
This uses [OkHttp](https://square.github.io/okhttp/) to perform the Http requests

## SpringClient
This uses [RestTemplate](https://docs.spring.io/spring-framework/docs/current/javadoc-api/org/springframework/web/client/RestTemplate.html) to perform the Http requests

__Note: the spring client gives an HTTP 500 error when trying to get resources after the token has been returned. I still need to fix this__

## Usage
Both clients implement the same interface so the usage is similar. Auth is handled internally so no need to worry about that

```Java
    // Create instance of the client and get a list of the accounts
    String clientId = "YOUR CLIENT ID";
    String secret = "YOUR SECRET";

    InvestecClient client = new InvestecClient(clientId, secret);

    List<Account> accounts = client.getAccounts();
```

## Examples

For full examples take a look at [OkHttpExample](Examples/src/main/java/za/co/investec/open/banking/examples/OkHttpExample.java) and [SpringExample](Examples/src/main/java/za/co/investec/open/banking/examples/SpringExample.java)

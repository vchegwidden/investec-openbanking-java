package za.co.investec.open.banking.okhttp;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.squareup.okhttp.*;
import za.co.investec.open.banking.domain.client.ApiClient;
import za.co.investec.open.banking.domain.exception.InvestecClientException;
import za.co.investec.open.banking.domain.model.AccessToken;
import za.co.investec.open.banking.domain.model.Account;
import za.co.investec.open.banking.domain.model.Balance;
import za.co.investec.open.banking.domain.model.Transaction;
import za.co.investec.open.banking.domain.request.AccessTokenRequest;
import za.co.investec.open.banking.domain.response.AccessTokenResponse;
import za.co.investec.open.banking.domain.response.AccountsResponse;
import za.co.investec.open.banking.domain.response.BalanceResponse;
import za.co.investec.open.banking.domain.response.TransactionsResponse;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class InvestecClient implements ApiClient {

    private static final String BASE_URL = "https://openapi.investec.com";

    private final String authHeader;
    private final OkHttpClient client;
    private final ObjectMapper objectMapper;

    private AccessToken accessToken;

    public InvestecClient(String clientId, String secret) {
        this.client = new OkHttpClient();
        this.objectMapper = new ObjectMapper();
        this.authHeader = createBasicAuth(clientId, secret);
    }

    public InvestecClient(String clientId, String secret, Integer timeout) {
        this.client = new OkHttpClient();
        this.objectMapper = new ObjectMapper();
        this.authHeader = createBasicAuth(clientId, secret);

        SetClientTimeout(timeout);
    }

    public InvestecClient(String clientId, String secret, OkHttpClient httpClient, ObjectMapper jacksonObjectMapper) {
        this.client = httpClient;
        this.objectMapper = jacksonObjectMapper;
        this.authHeader = createBasicAuth(clientId, secret);
    }

    private void SetClientTimeout(Integer timeout) {
        client.setConnectTimeout(timeout, TimeUnit.SECONDS);
        client.setReadTimeout(timeout, TimeUnit.SECONDS);
    }

    @Override
    public List<Account> getAccounts() {
        String url = String.format("%s/za/pb/v1/accounts", BASE_URL);
        AccessToken token = resolveAccessToken();
        String authHeader = createAuthHeaderString(token.getTokenType(), token.getAccessToken());

        try {
            Response response = get(url, createHeaders(authHeader));
            AccountsResponse accountsResponse = objectMapper.readValue(response.body().string(), AccountsResponse.class);

            return accountsResponse.getData().getAccount();
        } catch (IOException ioe) {
            throw new InvestecClientException(ioe);
        }
    }

    @Override
    public List<Transaction> getTransactions(String accountId) {
        String url = String.format("%s/za/pb/v1/accounts/%s/transactions", BASE_URL, accountId);
        AccessToken token = resolveAccessToken();
        String authHeader = createAuthHeaderString(token.getTokenType(), token.getAccessToken());

        try {
            Response response = get(url, createHeaders(authHeader));
            TransactionsResponse transactionsResponse = objectMapper.readValue(response.body().string(), TransactionsResponse.class);

            return transactionsResponse.getData().getTransactions();
        } catch (IOException ioe) {
            throw new InvestecClientException(ioe);
        }
    }

    @Override
    public Balance getBalance(String accountId) {
        String url = String.format("%s/za/pb/v1/accounts/%s/balance", BASE_URL, accountId);
        AccessToken token = resolveAccessToken();
        String authHeader = createAuthHeaderString(token.getTokenType(), token.getAccessToken());

        try {
            Response response = get(url, createHeaders(authHeader));
            BalanceResponse balanceResponse = objectMapper.readValue(response.body().string(), BalanceResponse.class);

            return balanceResponse.getData();
        } catch (IOException ioe) {
            throw new InvestecClientException(ioe);
        }
    }

    private AccessToken getAccessToken() {
        String url = String.format("%s/identity/v2/oauth2/token", BASE_URL);

        MediaType mediaType = MediaType.parse("application/x-www-form-urlencoded");
        RequestBody body = RequestBody.create(mediaType, new AccessTokenRequest().getUrlEncodedString());

        try {
            Response response = post(url, createHeaders(authHeader), body);
            return objectMapper.readValue(response.body().string(), AccessTokenResponse.class);
        } catch (IOException ioe) {
            throw new InvestecClientException(ioe);
        }
    }

    private AccessToken resolveAccessToken() {
        if (accessToken == null || !isTokenStillValid(accessToken)) {
            accessToken = getAccessToken();
        }

        return accessToken;
    }

    private boolean isTokenStillValid(AccessToken accessToken) {
        return LocalDateTime.now().isBefore(accessToken.getValidUntil());
    }

    private String createBasicAuth(String username, String password) {
        String auth = username + ":" + password;
        byte[] encodedAuth = Base64.getEncoder().encode(auth.getBytes(StandardCharsets.ISO_8859_1));
        return createAuthHeaderString("Basic", new String(encodedAuth));
    }

    private String createAuthHeaderString(String type, String token) {
        return String.format("%s %s", type, token);
    }

    private Headers createHeaders(String authHeader) {
        return new Headers.Builder()
                .add("Content-Type", "application/x-www-form-urlencoded")
                .add("Authorization", authHeader)
                .build();
    }

    private Response get(String url, Headers headers) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .get()
                .headers(headers)
                .build();
        return client.newCall(request).execute();
    }

    private Response post(String url, Headers headers, RequestBody body) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .method("POST", body)
                .headers(headers)
                .build();
        return client.newCall(request).execute();
    }
}

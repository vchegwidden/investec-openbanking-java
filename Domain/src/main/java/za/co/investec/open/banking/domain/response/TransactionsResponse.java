package za.co.investec.open.banking.domain.response;

import za.co.investec.open.banking.domain.model.ResponseLinks;
import za.co.investec.open.banking.domain.model.ResponseMeta;
import za.co.investec.open.banking.domain.model.Transactions;

public class TransactionsResponse extends BaseResponse<Transactions> {

    public TransactionsResponse() {
    }

    public TransactionsResponse(Transactions data, ResponseLinks links, ResponseMeta meta) {
        super(data, links, meta);
    }
}

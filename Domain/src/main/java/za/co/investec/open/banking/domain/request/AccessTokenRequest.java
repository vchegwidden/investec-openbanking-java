package za.co.investec.open.banking.domain.request;

import java.util.List;

public class AccessTokenRequest {

    private static final String GRANT_TYPE = "client_credentials";
    private final String scope;

    public AccessTokenRequest() {
        this.scope = "accounts";
    }

    public AccessTokenRequest(List<String> scopes) {
        if (scopes != null && !scopes.isEmpty()) {
            this.scope = "accounts";
        } else {
            this.scope = String.join(",", scopes);
        }
    }

    public String getGrantType() {
        return GRANT_TYPE;
    }

    public String getScope() {
        return scope;
    }

    public String getUrlEncodedString() {
        return "grant_type=" + GRANT_TYPE + "&scope=" + scope;
    }
}



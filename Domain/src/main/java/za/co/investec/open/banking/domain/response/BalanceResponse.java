package za.co.investec.open.banking.domain.response;

import za.co.investec.open.banking.domain.model.Balance;
import za.co.investec.open.banking.domain.model.ResponseLinks;
import za.co.investec.open.banking.domain.model.ResponseMeta;

public class BalanceResponse extends BaseResponse<Balance> {

    public BalanceResponse() {
    }

    public BalanceResponse(Balance data, ResponseLinks links, ResponseMeta meta) {
        super(data, links, meta);
    }
}

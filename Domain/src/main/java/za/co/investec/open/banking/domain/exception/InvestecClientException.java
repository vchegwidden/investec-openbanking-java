package za.co.investec.open.banking.domain.exception;

public class InvestecClientException extends RuntimeException {

    public InvestecClientException() {
        super();
    }

    public InvestecClientException(String message) {
        super(message);
    }

    public InvestecClientException(String message, Throwable cause) {
        super(message, cause);
    }

    public InvestecClientException(Throwable cause) {
        super(cause);
    }
}

package za.co.investec.open.banking.domain.client;

import za.co.investec.open.banking.domain.model.Account;
import za.co.investec.open.banking.domain.model.Balance;
import za.co.investec.open.banking.domain.model.Transaction;

import java.util.List;

public interface ApiClient {

    List<Account> getAccounts();

    List<Transaction> getTransactions(String accountId);

    Balance getBalance(String accountId);
}

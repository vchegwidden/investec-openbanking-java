package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Wrapper class for the list of transactions
 */
public class Transactions {

    @JsonProperty("transactions")
    private List<Transaction> transactionList;

    /**
     * Default constructor for Jackson
     */
    public Transactions() {
    }

    public List<Transaction> getTransactions() {
        return transactionList;
    }
}

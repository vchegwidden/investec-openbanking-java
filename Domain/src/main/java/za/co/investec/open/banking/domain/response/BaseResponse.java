package za.co.investec.open.banking.domain.response;

import za.co.investec.open.banking.domain.model.ResponseLinks;
import za.co.investec.open.banking.domain.model.ResponseMeta;

public abstract class BaseResponse<T> {

    private T data;
    private ResponseLinks links;
    private ResponseMeta meta;

    public BaseResponse() {
    }

    public BaseResponse(T data, ResponseLinks links, ResponseMeta meta) {
        this.data = data;
        this.links = links;
        this.meta = meta;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    public ResponseLinks getLinks() {
        return links;
    }

    public void setLinks(ResponseLinks links) {
        this.links = links;
    }

    public ResponseMeta getMeta() {
        return meta;
    }

    public void setMeta(ResponseMeta meta) {
        this.meta = meta;
    }
}

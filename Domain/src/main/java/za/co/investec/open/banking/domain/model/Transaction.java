package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import za.co.investec.open.banking.domain.converter.LocalDateDeserializer;
import za.co.investec.open.banking.domain.converter.LocalDateSerializer;

import java.math.BigDecimal;
import java.time.LocalDate;

public class Transaction {

    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("type")
    private String type;
    @JsonProperty("status")
    private String status;
    @JsonProperty("description")
    private String description;
    @JsonProperty("cardNumber")
    private String cardNumber;

    @JsonProperty("postingDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate postingDate;

    @JsonProperty("valueDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate valueDate;

    @JsonProperty("actionDate")
    @JsonDeserialize(using = LocalDateDeserializer.class)
    @JsonSerialize(using = LocalDateSerializer.class)
    private LocalDate actionDate;

    @JsonProperty("amount")
    private BigDecimal amount;

    /**
     * Default constructor for Jackson
     */
    public Transaction() {
    }

    public String getAccountId() {
        return accountId;
    }

    public String getType() {
        return type;
    }

    public String getStatus() {
        return status;
    }

    public String getDescription() {
        return description;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public LocalDate getPostingDate() {
        return postingDate;
    }

    public LocalDate getValueDate() {
        return valueDate;
    }

    public LocalDate getActionDate() {
        return actionDate;
    }

    public BigDecimal getAmount() {
        return amount;
    }
}

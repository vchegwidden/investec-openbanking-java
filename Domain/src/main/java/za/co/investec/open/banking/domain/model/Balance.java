package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;

public class Balance {

    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("currentBalance")
    private BigDecimal currentBalance;
    @JsonProperty("availableBalance")
    private BigDecimal availableBalance;
    @JsonProperty("currency")
    private String currency;

    /**
     * Default constructor for Jackson
     */
    public Balance() {
    }

    public String getAccountId() {
        return accountId;
    }

    public BigDecimal getCurrentBalance() {
        return currentBalance;
    }

    public BigDecimal getAvailableBalance() {
        return availableBalance;
    }

    public String getCurrency() {
        return currency;
    }
}

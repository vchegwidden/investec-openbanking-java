package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseMeta {

    @JsonProperty("totalPages")
    private int totalPages;

    /**
     * Default constructor for Jackson
     */
    public ResponseMeta() {
    }

    public int getTotalPages() {
        return totalPages;
    }
}
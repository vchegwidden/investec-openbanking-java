package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

/**
 * Wrapper class for the list of accounts
 */
public class Accounts {

    @JsonProperty("accounts")
    private List<Account> accountList;

    /**
     * Default constructor for Jackson
     */
    public Accounts() {
    }

    public List<Account> getAccount() {
        return accountList;
    }
}

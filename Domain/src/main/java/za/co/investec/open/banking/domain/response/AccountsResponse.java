package za.co.investec.open.banking.domain.response;

import za.co.investec.open.banking.domain.model.Accounts;
import za.co.investec.open.banking.domain.model.ResponseLinks;
import za.co.investec.open.banking.domain.model.ResponseMeta;

public class AccountsResponse extends BaseResponse<Accounts> {

    public AccountsResponse() {
    }

    public AccountsResponse(Accounts data, ResponseLinks links, ResponseMeta meta) {
        super(data, links, meta);
    }
}

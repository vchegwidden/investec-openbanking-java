package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class Account {

    @JsonProperty("accountId")
    private String accountId;
    @JsonProperty("accountNumber")
    private String accountNumber;
    @JsonProperty("accountName")
    private String accountName;
    @JsonProperty("referenceName")
    private String referenceName;
    @JsonProperty("productName")
    private String productName;

    /**
     * Default constructor for Jackson
     */
    public Account() {
    }

    public String getAccountId() {
        return accountId;
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public String getAccountName() {
        return accountName;
    }

    public String getReferenceName() {
        return referenceName;
    }

    public String getProductName() {
        return productName;
    }
}

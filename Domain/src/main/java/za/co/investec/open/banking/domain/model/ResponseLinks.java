package za.co.investec.open.banking.domain.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ResponseLinks {

    @JsonProperty("self")
    private String self;

    /**
     * Default constructor for Jackson
     */
    public ResponseLinks() {
    }

    public String getSelf() {
        return self;
    }
}
package za.co.investec.open.banking.domain.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AccountsResponseTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void parseFromJson() {
        try {
            String json = TestUtility.readFile("AccountsResponse.json");
            AccountsResponse accountsResponse = objectMapper.readValue(json, AccountsResponse.class);

            assertEquals(2, accountsResponse.getData().getAccount().size());

            assertEquals("0000000000000000000000000", accountsResponse.getData().getAccount().get(0).getAccountId());
            assertEquals("10000000001", accountsResponse.getData().getAccount().get(0).getAccountNumber());
            assertEquals("Mr John Doe", accountsResponse.getData().getAccount().get(0).getAccountName());
            assertEquals("Mr J Doe", accountsResponse.getData().getAccount().get(0).getReferenceName());
            assertEquals("Private Bank Account", accountsResponse.getData().getAccount().get(0).getProductName());

            assertEquals("0000000000000000000000001", accountsResponse.getData().getAccount().get(1).getAccountId());
            assertEquals("10000000002", accountsResponse.getData().getAccount().get(1).getAccountNumber());
            assertEquals("Mr John Doe", accountsResponse.getData().getAccount().get(1).getAccountName());
            assertEquals("Mr J Doe", accountsResponse.getData().getAccount().get(1).getReferenceName());
            assertEquals("Cash Management Account", accountsResponse.getData().getAccount().get(1).getProductName());

            assertEquals("https://openapi.investec.com/za/pb/v1/accounts", accountsResponse.getLinks().getSelf());
            assertEquals(1, accountsResponse.getMeta().getTotalPages());

        } catch (JsonProcessingException ex) {
            fail();
        }
    }
}
package za.co.investec.open.banking.domain.response;

import java.io.IOException;
import java.io.InputStream;
import java.util.Scanner;

public class TestUtility {

    public static String readFile(String path) {
        try (InputStream inputStream = ClassLoader.getSystemClassLoader().getResourceAsStream(path)) {
            if (inputStream != null) {
                Scanner s = new Scanner(inputStream).useDelimiter("\\A");
                String result = s.hasNext() ? s.next() : "";
                return result;
            }
        } catch (IOException ex) {
            return null;
        }
        
        return null;
    }
}

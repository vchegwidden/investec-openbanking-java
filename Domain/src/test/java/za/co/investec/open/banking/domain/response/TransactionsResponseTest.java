package za.co.investec.open.banking.domain.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.math.BigDecimal;
import java.time.LocalDate;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class TransactionsResponseTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void parseFromJson() {
        try {
            String json = TestUtility.readFile("TransactionsResponse.json");
            TransactionsResponse transactionsResponse = objectMapper.readValue(json, TransactionsResponse.class);

            assertEquals(3, transactionsResponse.getData().getTransactions().size());

            assertEquals("0000000000000000000000000", transactionsResponse.getData().getTransactions().get(0).getAccountId());
            assertEquals("DEBIT", transactionsResponse.getData().getTransactions().get(0).getType());
            assertEquals("POSTED", transactionsResponse.getData().getTransactions().get(0).getStatus());
            assertEquals("MSFT AZURE", transactionsResponse.getData().getTransactions().get(0).getDescription());
            assertEquals("404444xxxxxx4444", transactionsResponse.getData().getTransactions().get(0).getCardNumber());
            assertEquals(LocalDate.parse("2020-07-09"), transactionsResponse.getData().getTransactions().get(0).getPostingDate());
            assertEquals(LocalDate.parse("2020-07-15"), transactionsResponse.getData().getTransactions().get(0).getValueDate());
            assertEquals(LocalDate.parse("2020-07-10"), transactionsResponse.getData().getTransactions().get(0).getActionDate());
            assertEquals(new BigDecimal("372.13"), transactionsResponse.getData().getTransactions().get(0).getAmount());

            assertEquals("0000000000000000000000000", transactionsResponse.getData().getTransactions().get(1).getAccountId());
            assertEquals("DEBIT", transactionsResponse.getData().getTransactions().get(1).getType());
            assertEquals("POSTED", transactionsResponse.getData().getTransactions().get(1).getStatus());
            assertEquals("N1 GRASMERE GRASMERE ZA", transactionsResponse.getData().getTransactions().get(1).getDescription());
            assertEquals("404444xxxxxx4444", transactionsResponse.getData().getTransactions().get(1).getCardNumber());
            assertEquals(LocalDate.parse("2020-07-07"), transactionsResponse.getData().getTransactions().get(1).getPostingDate());
            assertEquals(LocalDate.parse("2020-07-15"), transactionsResponse.getData().getTransactions().get(1).getValueDate());
            assertEquals(LocalDate.parse("2020-07-10"), transactionsResponse.getData().getTransactions().get(1).getActionDate());
            assertEquals(new BigDecimal("21"), transactionsResponse.getData().getTransactions().get(1).getAmount());

            assertEquals("0000000000000000000000000", transactionsResponse.getData().getTransactions().get(2).getAccountId());
            assertEquals("CREDIT", transactionsResponse.getData().getTransactions().get(2).getType());
            assertEquals("POSTED", transactionsResponse.getData().getTransactions().get(2).getStatus());
            assertEquals("CREDIT INTEREST", transactionsResponse.getData().getTransactions().get(2).getDescription());
            assertEquals("", transactionsResponse.getData().getTransactions().get(2).getCardNumber());
            assertEquals(LocalDate.parse("2020-04-16"), transactionsResponse.getData().getTransactions().get(2).getPostingDate());
            assertEquals(LocalDate.parse("2020-04-15"), transactionsResponse.getData().getTransactions().get(2).getValueDate());
            assertEquals(LocalDate.parse("2020-07-10"), transactionsResponse.getData().getTransactions().get(2).getActionDate());
            assertEquals(new BigDecimal("0.3"), transactionsResponse.getData().getTransactions().get(2).getAmount());

            assertEquals("https://openapi.investec.com/za/pb/v1/accounts/0000000000000000000000000/transactions", transactionsResponse.getLinks().getSelf());
            assertEquals(1, transactionsResponse.getMeta().getTotalPages());

        } catch (JsonProcessingException ex) {
            fail();
        }
    }
}
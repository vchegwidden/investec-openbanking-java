package za.co.investec.open.banking.domain.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class AccessTokenResponseTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void parseFromJson() {
        try {
            String json = TestUtility.readFile("AccessTokenResponse.json");
            AccessTokenResponse accessTokenResponse = objectMapper.readValue(json, AccessTokenResponse.class);

            assertEquals("mytoken", accessTokenResponse.getAccessToken());
            assertEquals("Bearer", accessTokenResponse.getTokenType());
            assertEquals(1799, accessTokenResponse.getExpiresIn().intValue());
            assertEquals("accounts", accessTokenResponse.getScope());

        } catch (JsonProcessingException ex) {
            fail();
        }
    }
}
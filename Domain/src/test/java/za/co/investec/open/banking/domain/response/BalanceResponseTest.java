package za.co.investec.open.banking.domain.response;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Test;

import java.math.BigDecimal;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

public class BalanceResponseTest {

    ObjectMapper objectMapper = new ObjectMapper();

    @Test
    public void parseFromJson() {
        try {
            String json = TestUtility.readFile("BalanceResponse.json");
            BalanceResponse balanceResponse = objectMapper.readValue(json, BalanceResponse.class);

            assertEquals("0000000000000000000000000", balanceResponse.getData().getAccountId());
            assertEquals(new BigDecimal("10000.50"), balanceResponse.getData().getCurrentBalance());
            assertEquals(new BigDecimal("19999.99"), balanceResponse.getData().getAvailableBalance());
            assertEquals("ZAR", balanceResponse.getData().getCurrency());

            assertEquals("https://openapi.investec.com/za/pb/v1/accounts/0000000000000000000000000/balance", balanceResponse.getLinks().getSelf());
            assertEquals(1, balanceResponse.getMeta().getTotalPages());

        } catch (JsonProcessingException ex) {
            fail();
        }
    }
}
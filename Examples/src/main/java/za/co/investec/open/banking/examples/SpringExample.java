package za.co.investec.open.banking.examples;

import za.co.investec.open.banking.domain.model.Account;
import za.co.investec.open.banking.domain.model.Balance;
import za.co.investec.open.banking.domain.model.Transaction;
import za.co.investec.open.banking.spring.InvestecClient;

import java.util.List;

public class SpringExample {

    public static void main(String[] args) {

        String clientId = "YOUR CLIENT ID";
        String secret = "YOUR SECRET";
        InvestecClient client = new InvestecClient(clientId, secret, 25);

        try {
            List<Account> accounts = client.getAccounts();

            if (accounts != null && !accounts.isEmpty()) {
                Balance balance = client.getBalance(accounts.get(0).getAccountId());
                List<Transaction> transactions = client.getTransactions(accounts.get(0).getAccountId());
                transactions.size();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

package za.co.investec.open.banking.spring;

import org.springframework.http.*;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;
import za.co.investec.open.banking.domain.client.ApiClient;
import za.co.investec.open.banking.domain.model.AccessToken;
import za.co.investec.open.banking.domain.model.Account;
import za.co.investec.open.banking.domain.model.Balance;
import za.co.investec.open.banking.domain.model.Transaction;
import za.co.investec.open.banking.domain.request.AccessTokenRequest;
import za.co.investec.open.banking.domain.response.AccessTokenResponse;
import za.co.investec.open.banking.domain.response.AccountsResponse;
import za.co.investec.open.banking.domain.response.BalanceResponse;
import za.co.investec.open.banking.domain.response.TransactionsResponse;

import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.List;

public class InvestecClient implements ApiClient {

    private static final String BASE_URL = "https://openapi.investec.com";

    private final String username;
    private final String password;
    private final RestTemplate restTemplate;

    private AccessToken accessToken;

    public InvestecClient(String clientId, String secret) {
        this.restTemplate = Config.getDefaultRestTemplate();
        this.username = clientId;
        this.password = secret;
    }

    public InvestecClient(String clientId, String secret, Integer timeout) {
        this.restTemplate = Config.getDefaultRestTemplate(timeout);
        this.username = clientId;
        this.password = secret;
    }

    public InvestecClient(String clientId, String secret, RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
        this.username = clientId;
        this.password = secret;
    }

    @Override
    public List<Account> getAccounts() {
        String url = buildUrl("/za/pb/v1/accounts");

        HttpHeaders headers = createBearerAuth();
        HttpEntity<MultiValueMap<String, String>> requestHttpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<AccountsResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestHttpEntity, AccountsResponse.class);

        return response.getBody().getData().getAccount();
    }

    @Override
    public List<Transaction> getTransactions(String accountId) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("accountNumber", accountId);
        String url = buildUrl("/za/pb/v1/accounts/{accountNumber}/transactions", params);

        HttpHeaders headers = createBearerAuth();
        HttpEntity<MultiValueMap<String, String>> requestHttpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<TransactionsResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestHttpEntity, TransactionsResponse.class);

        return response.getBody().getData().getTransactions();
    }

    @Override
    public Balance getBalance(String accountId) {
        MultiValueMap<String, String> params = new LinkedMultiValueMap<>();
        params.add("accountNumber", accountId);
        String url = buildUrl("/za/pb/v1/accounts/{accountNumber}/balance", params);

        HttpHeaders headers = createBearerAuth();
        HttpEntity<MultiValueMap<String, String>> requestHttpEntity = new HttpEntity<>(null, headers);
        ResponseEntity<BalanceResponse> response = restTemplate.exchange(url, HttpMethod.GET, requestHttpEntity, BalanceResponse.class);

        return response.getBody().getData();
    }

    private AccessToken getAccessToken() {
        String url = buildUrl("/identity/v2/oauth2/token");

        HttpHeaders headers = createBasicAuth(username, password);
        AccessTokenRequest accessTokenRequest = new AccessTokenRequest();
        MultiValueMap<String, String> map = new LinkedMultiValueMap<>();
        map.add("grant_type", accessTokenRequest.getGrantType());
        map.add("scope", accessTokenRequest.getScope());

        HttpEntity<MultiValueMap<String, String>> requestHttpEntity = new HttpEntity<>(map, headers);
        ResponseEntity<AccessTokenResponse> response = restTemplate.postForEntity(url, requestHttpEntity, AccessTokenResponse.class);

        return response.getBody();
    }

    private AccessToken resolveAccessToken() {
        if (accessToken == null || !isTokenStillValid(accessToken)) {
            accessToken = getAccessToken();
        }

        return accessToken;
    }

    private boolean isTokenStillValid(AccessToken accessToken) {
        return LocalDateTime.now().isBefore(accessToken.getValidUntil());
    }

    private String buildUrl(String operation) {
        return UriComponentsBuilder.fromHttpUrl(BASE_URL + operation)
                .build()
                .toString();
    }

    private String buildUrl(String operation, MultiValueMap<String, String> params) {
        return UriComponentsBuilder.fromHttpUrl(BASE_URL + operation)
                .queryParams(params)
                .build()
                .toString();
    }

    private HttpHeaders createBasicAuth(String username, String password) {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBasicAuth(username, password, StandardCharsets.ISO_8859_1);
        return headers;
    }

    private HttpHeaders createBearerAuth() {
        AccessToken token = resolveAccessToken();
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_FORM_URLENCODED);
        headers.setBearerAuth(token.getAccessToken());
        return headers;
    }
}

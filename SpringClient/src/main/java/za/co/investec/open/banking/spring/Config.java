package za.co.investec.open.banking.spring;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.MediaType;
import org.springframework.http.client.HttpComponentsClientHttpRequestFactory;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.client.RestTemplate;

import java.util.Arrays;
import java.util.Collections;

public class Config {

    public static RestTemplate getDefaultRestTemplate() {
        RestTemplate restTemplate = new RestTemplate();
        restTemplate.setMessageConverters(Arrays.asList(getDefaultJacksonConverter(), getDefaultFormHttpMessageConverter()));

        return restTemplate;
    }

    public static RestTemplate getDefaultRestTemplate(int timeout) {
        HttpComponentsClientHttpRequestFactory factory = new HttpComponentsClientHttpRequestFactory();
        factory.setReadTimeout(timeout * 1000);
        factory.setConnectTimeout(timeout * 1000);

        RestTemplate restTemplate = new RestTemplate(factory);
        restTemplate.setMessageConverters(Arrays.asList(getDefaultJacksonConverter(), getDefaultFormHttpMessageConverter()));

        return restTemplate;
    }

    public static MappingJackson2HttpMessageConverter getDefaultJacksonConverter() {
        MappingJackson2HttpMessageConverter jacksonConverter = new MappingJackson2HttpMessageConverter();
        jacksonConverter.setObjectMapper(new ObjectMapper());
        return jacksonConverter;
    }

    public static FormHttpMessageConverter getDefaultFormHttpMessageConverter() {
        FormHttpMessageConverter formHttpMessageConverter = new FormHttpMessageConverter();
        formHttpMessageConverter.setSupportedMediaTypes(Collections.singletonList(MediaType.APPLICATION_FORM_URLENCODED));
        return formHttpMessageConverter;
    }
}
